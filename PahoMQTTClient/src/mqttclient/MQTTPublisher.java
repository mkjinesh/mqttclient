package mqttclient;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttClientPersistence;
import org.eclipse.paho.client.mqttv3.MqttDefaultFilePersistence;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttTopic;

/***
 * MQTT publisher
 * @author Jinesh M.K
 * @mail mkjinesh@gmail.com
 * 
 */
public class MQTTPublisher {

	static final String BROKER_URL = "tcp://mymqttbroker.net:1883";//Replace with your mqtt broker url
	//static final String BROKER_URL = "tcp://test.mosquitto.org:1883";//public mosquitto server
	static final String TOPIC = "mqtt/test"; //Change according to your application

	public static void main(String args[]) {

		try {
			// Creating new default persistence for mqtt client
			MqttClientPersistence persistence = new MqttDefaultFilePersistence(
					"/tmp");

			// mqtt client with specific url and client id
			MqttClient client = new MqttClient(BROKER_URL, "Publisher-ID",
					persistence);
			client.connect();

			MqttTopic myTopic = client.getTopic(TOPIC);
			String msg = null;
			System.out
					.println("Enter the message to publish,Type quit to exit\n");
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));
			msg = br.readLine();
			while (!msg.equals("quit")) {
				myTopic.publish(new MqttMessage(msg.getBytes()));
				System.out.println("Message published on" + TOPIC);
				msg = br.readLine();
			}
			client.disconnect();
		} catch (MqttException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
}
